![image](https://img-c.udemycdn.com/course/750x422/2777074_6d96_11.jpg)


# 🚀 Spring Cloud and RabbitmQ

Bem-vindo(a). Este é o projeto de estudos integrando Spring Cloud and RabbitmQ!

O objetivo desta aplicação é criar uma integração de varios serviços spring-boot, que utilizam funcionalidades da spring-cloud, como service-discovery e gateway.

Alem de utilizar tanto a comunicação sincrona com Open Feign e Assincrona com RabbitMQ.

# 🧠 Contexto

Tecnologias utilizadas neste projeto:
-  Java v17
-  Spring-Boot v2.6
-  Flyway (para as migrations)
-  JPA / Hibernate para a rotinas de banco de dados
-  Mysql
-  Docker
-  Service discovery utilizando o Eureka
-  API Gateway e load balancer no projeto utilizando o Gateway da cloud
-  Comunicação sincrona com Open Feign
-  Circuit breaker e fallback
-  Comunicação Assincrona com RabbitMQ
-  Tratamento de falhas no consumo de mensagem

## 📋 Instruções para subir a aplicação utilizando Docker-compose

É necessário ter o Docker e Docker-compose instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute os comandos abaixo:
- docker-compose pull
- docker-compose up -build
- Vc terá novos container rodando na sua maquina.
- A API Gateway vao rodar por padrão na porta 8082.
- Abaixo nesta doc tem a documentação das rotas.


## ✔️ Rotas da API

Alem da breve descrição abaixo sobre as rotas tbm há um link no fim para download da collection que poderá ser importada no seu postman ou insomnia:

# PAGAMENTOS
- GET    /pagamentos
- GET    /pagamentos/{id}
- PUT    /pagamentos/{id}
- POST   /pagamentos
- DELETE /pagamentos/{id}
- PATCH  /pagamentos/{id}/confirmar
# PEDIDOS
- GET    /pedidos
- GET    /pedidos/{id}
- POST   /pedidos

- link para download da collection feita no postman que pode apox feito o download pode ser importada no postman ou insomnia (https://gitlab.com/marceloeduardo244/spring-cloud-and-rabbitmq/-/blob/main/docs/ALURA-FOOD.postman_collection.json) 

# Video testando a mensageria
- link Utilizando o sistema (https://gitlab.com/marceloeduardo244/spring-cloud-and-rabbitmq/-/blob/main/docs/utilizando-rabbit-mq-com-spring-boot.mp4)

## Esquema de conexão do Docker compose
- https://gitlab.com/marceloeduardo244/spring-cloud-and-rabbitmq/-/blob/main/docs/arquitetura_da_stack_docker_compose.jpg
- https://drive.google.com/file/d/18Y2Jis5n5KX_bdF8RDXq-I4NkLdDe2dz/view?usp=sharing

Made with 💜 at OliveiraDev